
#include <msp430.h>
#include <string.h>
#include "command.h"
#include "define.h"
#include "deplacement.h"

/*
 * Prototypes
 */

uchar_32 cmd[CMDLEN];      /* tableau de caracteres lie a la commande user*/

/* ----------------------------------------------------------------------------
 * Fonction d'interpretation des commandes utilisateur
 * Entrees: -
 * Sorties:  -
 */


void interpreteurblut( void )
{
    if(strcmp((const char  *)cmd, "h") == 0)          /*----------------------------------- help*/
    {
        envoi_msg_UART("\r\nCommandes :");
        envoi_msg_UART("\r\n'ver' : version");
        envoi_msg_UART("\r\n'a' : advance");
        envoi_msg_UART("\r\n'b' : back");
        envoi_msg_UART("\r\n'l' : left");
        envoi_msg_UART("\r\n'r' : right");
        envoi_msg_UART("\r\n's' : stop");
        envoi_msg_UART("\r\n'v' : start servo");
        envoi_msg_UART("\r\n't' : stop servo");
        envoi_msg_UART("\r\n'h' : help\r\n");
    }
    else if (strcmp((const char  *)cmd, "v") == 0)
      {
          envoi_msg_UART("\r\n");
          envoi_msg_UART((const char  *)cmd);
          envoi_msg_UART("->");
          Send_char_SPI(0x31); /* Send '0' over SPI to Slave*/
          envoi_msg_UART("\r\n");
      }
      else if (strcmp((const char  *)cmd, "t") == 0)
      {
          envoi_msg_UART("\r\n");
          envoi_msg_UART((const char  *)cmd);
          envoi_msg_UART("->");
          Send_char_SPI(0x30); /* Send '1' over SPI to Slave*/
          envoi_msg_UART("\r\n");
      }
    else if (strcmp((const char  *)cmd, "a") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((const char  *)cmd);
        envoi_msg_UART("->");
        Avancer();
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char  *)cmd, "b") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((const char  *)cmd);
        envoi_msg_UART("->");
        Reculer();
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char  *)cmd, "l") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((const char  *)cmd);
        envoi_msg_UART("->");
        TournerGaucheM();
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char  *)cmd, "r") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((const char  *)cmd);
        envoi_msg_UART("->");
        TournerDroiteM();
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char  *)cmd, "s") == 0)
   {
       envoi_msg_UART("\r\n");
       envoi_msg_UART((const char  *)cmd);
       envoi_msg_UART("->");
       Stop();
       envoi_msg_UART("\r\n");
   }
    else
    {

        envoi_msg_UART("\r\n ?");
        envoi_msg_UART((const char  *)cmd);
    }

    envoi_msg_UART(PROMPT);        /*---------------------------- command prompt*/
}






