

#include <msp430.h> 
#include <string.h>
#include "command.h"
#include "define.h"
#include "deplacement.h"
/*
 * Variables globales
 */

uchar_32 cmd[CMDLEN];      /* tableau de caracteres lie a la commande user*/
uchar_32 car = 0x30;       /*0*/
uint_32  nb_car = 0;
uchar_32 intcmd = FALSE;   /* call interpreteur()*/
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * main.c
 */
int main( void )
{
    init_BOARD();
    init_UART();
    init_USCI();
    initr();

    envoi_msg_UART("\rReady !\r\n"); /* user prompt*/
    envoi_msg_UART(PROMPT);        /*---------------------------- command prompt*/

 while(1)
{
    if( intcmd>0 )
    {
        while ((UCB0STAT & UCBUSY)>0){};   /* attend que USCI_SPI soit dispo.*/
        /*interpreteur();          execute la commande utilisateur*/
        interpreteurblut();
        intcmd = FALSE;         /* acquitte la commande en cours*/
    }
    else
    {
        __bis_SR_register(LPM4_bits | GIE); /* general interrupts enable & Low Power Mode*/
    }
}
}

/* --------------------------- R O U T I N E S   D ' I N T E R R U P T I O N S */

/* ************************************************************************* */
/* VECTEUR INTERRUPTION USCI RX                                              */
/* ************************************************************************* */

#pragma vector = USCIAB0RX_VECTOR

__interrupt void USCIAB0RX_ISR(void)
{
    /*---------------- UART*/
    if ((IFG2 & UCA0RXIFG)>0)
    {
        while((IFG2 & UCA0RXIFG)<1){};
        cmd[nb_car]=UCA0RXBUF;         /* lecture caract�re re�u*/

        while((IFG2 & UCA0TXIFG)>0){};    /* attente de fin du dernier envoi (UCA0TXIFG � 1 quand UCA0TXBUF vide) / echo */
        UCA0TXBUF = cmd[nb_car];

        if( cmd[nb_car] == ESC)
        {
            nb_car = 0U;
            cmd[1] = 0x00;
            cmd[0] = CR;
        }

        if( (cmd[nb_car] == CR) || (cmd[nb_car] == LF))
        {
            cmd[nb_car] = 0x00;
            intcmd = TRUE;
            nb_car = 0U;
            __bic_SR_register_on_exit(LPM4_bits);   /* OP mode !*/
        }
        else if( (nb_car < CMDLEN) && (!((cmd[nb_car] == BSPC) || (cmd[nb_car] == DEL))) )
        {
            nb_car++;
        }
        else
        {
            cmd[nb_car] = 0x00;
            nb_car--;
        }

    }

    /*--------------- SPI*/
    else if ((IFG2 & UCB0RXIFG)>0)
    {
        while((((UCB0STAT & UCBUSY)>0) && ((UCB0STAT & UCOE)<1)>0)){};
        while((IFG2 & UCB0RXIFG)<0){};
        cmd[0] = UCB0RXBUF;
        cmd[1] = 0x00;
        P1OUT ^= LED_R;
    }
	else{ P1OUT ^= LED_R; }
return 0;
}
/*------------------------------------------------------------------ End ISR*/
