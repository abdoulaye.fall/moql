
#include "deplacement.h"
#include "ADC.h"


/*définition globale*/
#define CMDLEN  12               /* longueur maximum de la commande utilisateur*/
#define LF      0x0A
#define CR      0x0D

#define Led_Temoin_2            BIT0  /*P1.0*/
#define Bouton_Depart           BIT3  /*P1.3*/
#define Capt_Infra_Rouge        BIT5  /*P1.5*/
#define Led_Temoin_1            BIT6  /*P1.6*/

#define Opto_Coupl_Mot_G        BIT0  /*P2.0*/
#define Sens_Mot_G              BIT1  /*P2.1*/
#define Mot_G                   BIT2  /*P2.2*/

#define Opto_Coupl_Mot_D        BIT3  /*P2.3*/
#define Mot_D                   BIT4  /*P2.4*/
#define Sens_Mot_D              BIT5  /*P2.5*/

/*Seuil de comparaison*/
#define SeuilIR 400U                    /*seuil de detection d'obstacle(environ 6cm)*/

/* -------------fonction d'initialisation de l'engin ---------------- */

void initr(void)
{
    /*Initialisation de l'ADC*/
    ADC_init();

    /*Mise des capteurs en entree du Port P1*/
    P1DIR &= ~(Capt_Infra_Rouge | Bouton_Depart);

    /*Mise des leds en sortie du Port P1*/
    P1DIR |= Led_Temoin_1 | Led_Temoin_2;

    /*Mise des caractéristiques des moteurs (sens,alimentation,opto-coupleur) en sortie du Port P2*/
    P2DIR |= (Sens_Mot_G + Mot_G + Mot_D + Sens_Mot_D);
    P2DIR |= (Opto_Coupl_Mot_G + Opto_Coupl_Mot_D);

    /*Pour PWM*/
    P2SEL |= (Mot_G + Mot_D);
    P2SEL2 &= ~(Mot_G + Mot_D);

    /*Reglagles Bouton Depart*/
    P1REN |= Bouton_Depart;
    P1OUT |= Bouton_Depart;

    /*Leds eteintes*/
    P1OUT &= ~(Led_Temoin_1 | Led_Temoin_2);

    /*sens de l'avancee des deux moteurs*/
    P2OUT &= ~(Sens_Mot_G) ;
    P2OUT |= Sens_Mot_D;

    /* set SMCLK (Sub Main Clock) to 1MHz*/
    DCOCTL = 0;
    DCOCTL = CALDCO_1MHZ;  BCSCTL1 = CALBC1_1MHZ;

    /* set TimerA-1 period at 1250Hz (1MHz/8/100)*/
    TA1CTL    = TASSEL_2 | ID_3 | MC_1 | 0;  TA1CCR0 = 100;

    /* set TimerA-1.1 set/reset output mode  compare CCR1 (0% of CCR0) and reset*/
    TA1CCTL1 |= OUTMOD_7;                    TA1CCR1 = 0;       /*moteur gauche eteint*/


    TA1CCTL2 |= OUTMOD_7;                   
     TA1CCR2 = 0;       /*moteur droit eteint*/

    P1DIR |= BIT0 | BIT6;  // port 1.0  en sortie
    P1OUT &= ~(BIT0 | BIT6);  // force etat bas P1.0 - LED1
    P1REN |= BIT6;

}


/*Fonction avancer*/
void Avancer(void)
{
    P2OUT &= ~Sens_Mot_G;
    P2OUT |= (Sens_Mot_D) ;
    TA1CCR1 = 150;   TA1CCR2 = 150;
}

/* fonction reculer*/
void Reculer(void)
{
    P2OUT |= Sens_Mot_G;
    P2OUT &= ~(Sens_Mot_D) ;
    TA1CCR1 = 100;   TA1CCR2 = 100;
}

/*fonction redresser droite*/
void TournerDroiteM(void)
{
    P2OUT |= Sens_Mot_G;
    P2OUT |= (Sens_Mot_D) ;
    TA1CCR1 = 50;   TA1CCR2 = 50;
    __delay_cycles(100000);
    TA1CCR1 = 0;   TA1CCR2 = 0;


}
/*fonction redresser gauche*/
void TournerGaucheM(void)
{
    P2OUT &= ~Sens_Mot_G;
    P2OUT &= ~(Sens_Mot_D) ;
    TA1CCR1 = 50;   TA1CCR2 = 50;
    __delay_cycles(100000);
    TA1CCR1 = 0;   TA1CCR2 = 0;

}
/*fonction arrêt*/
void Stop(void)
{
    TA1CCR1 = 0;   TA1CCR2 = 0;
}


/*fonction de détection de l'appui sur le bouton*/
uint_32 isStartBoutonPressed(void)
{
    if ( (P1IN & Bouton_Depart) == 0 )              /*bouton appuye*/
	{
        return 1;
}
    else
{
        return 0;
}
}











