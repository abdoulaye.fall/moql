/*
 * deplacement.h
 *
 *  Created on: 10 mars 2021
 *      Author: layef
 */

/*declaration de variables globales*/

/*ensemble des etats*/
#include "ADC.h"
#include "msp430.h"

void initr(void);

void Avancer(void);
/* fonction reculer*/
void Reculer(void);
/*fonction redresser droite*/
void TournerDroite(void);
/*fonction redresser gauche*/
void TournerGauche(void);
/*fonction arr�t*/
void Stop(void);
