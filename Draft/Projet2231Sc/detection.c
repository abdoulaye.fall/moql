/*
 * detection.c
 *
 *  Created on: 11 mars 2021
 *      Author: layef
 */
#include "msp430.h"
#include "ADC.h"
#include "detection.h"
#define  SeuilIR 500

int detectionObstacle()
{

    //lecture valeur renvoyee par l'IR
    ADC_Demarrer_conversion(0x04);
    int IR = ADC_Lire_resultat();

    if ( IR >= 500 )  //'obstacle devant
    {
        return 1;
    }

    else
    {
        return 0; //pas obstacle devant
    }


}


