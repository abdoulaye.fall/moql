################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnk_msp430g2231.cmd 

C_SRCS += \
../ADC.c \
../detection.c \
../main.c \
../servo.c \
../spi.c 

C_DEPS += \
./ADC.d \
./detection.d \
./main.d \
./servo.d \
./spi.d 

OBJS += \
./ADC.obj \
./detection.obj \
./main.obj \
./servo.obj \
./spi.obj 

OBJS__QUOTED += \
"ADC.obj" \
"detection.obj" \
"main.obj" \
"servo.obj" \
"spi.obj" 

C_DEPS__QUOTED += \
"ADC.d" \
"detection.d" \
"main.d" \
"servo.d" \
"spi.d" 

C_SRCS__QUOTED += \
"../ADC.c" \
"../detection.c" \
"../main.c" \
"../servo.c" \
"../spi.c" 


