/*
 * uart.h
 *
 *  Created on: 10 mars 2021
 *      Author: layef
 */

#include"msp430.h"

void init();
void init_uart(void);
void interruption(void);
void sendata(unsigned char data);
void sendString(char data[]);

