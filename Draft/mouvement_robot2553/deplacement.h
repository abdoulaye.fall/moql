/*
 * deplacement.h
 *
 *  Created on: 10 mars 2021
 *      Author: layef
 */

/*declaration de variables globales*/

/*ensemble des etats*/
#include "ADC.h"
#include "msp430.h"

void initr();

void Avancer();
/* fonction reculer*/
void Reculer();
/*fonction redresser droite*/
void TournerDroite();
/*fonction redresser gauche*/
void TournerGauche();
/*fonction arr�t*/
void Stop();
