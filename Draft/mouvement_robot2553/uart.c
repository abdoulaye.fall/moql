/*
 * uart.c
 *
 *  Created on: 10 mars 2021
 *      Author: layef
 */

#include "uart.h"





void init(void)
{
    WDTCTL = WDTPW + WDTHOLD;   /*stop watchdog timer*/

    /* Use Calibration values for 1MHz Clock DCO*/
    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;
}
void init_uart(void)
{

    /* Configure Pin Muxing P1.1 RXD and P1.2 TXD */
     P1SEL = BIT1 | BIT2 ;
     P1SEL2 = BIT1 | BIT2;
     /*configure*/
     UCA0CTL1 = UCSWRST; /*reset*/
     UCA0CTL1 |= UCSSEL_2; /*SMCLK*/
     UCA0BR0 = 104; /*vitesse de transmission 9600 1MHz*/
     UCA0BR1 = 0;   /*vitesse de transmission 9600 1MHz*/
     UCA0MCTL = UCBRS0 ;/*modulation*/
     UCA0CTL1 &=~UCSWRST; /*end reset*/
}
void interruption(void)
{
    IFG2 &= ~UCA0RXIFG; /*Clear RX flag*/
    IFG2 &= ~UCA0TXIFG; /* Clear RX flag*/
    IE2 |= UCA0RXIE;  /*Enable the Receive  interrupt*/
    IE2 |= UCA0TXIE;  /* Enable the Transmit  interrupt*/
}
void sendata(unsigned char data)
{
    while (!(IFG2&UCA0TXIFG)) ; /* USCI_A0 TX buffer ready, si um send data pc could not receive data from pc?*/

    UCA0TXBUF = data;
}
void sendString(char *data)
{
    unsigned int i=0;
    int len=strlen(data);


    for (i=0; i<len+1; i++)
     {
         sendata(data[i]);

     }



}


