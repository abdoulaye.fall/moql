#include <msp430.h> 
#include "uart.h"
#include "deplacement.h"


/**
 * main.c
 */
char data1[]={"\rles-commande\r\n"};
char data2[]={"\ra:-->Avancer\r\nb:-->Reculer\r\nc:-->Tourner a gauche\
        \r\n"};
char data3[]={"\rd:-->Tourner a droite\r\ne:-->Arret \r\nh:-->help\r\n"};
int main(void)
{
    P1DIR  |= BIT6 + BIT0;  /* P1.6 output*/
    P1OUT  &= ~(BIT6+BIT0);  /*P1.6 = 0*/
    init();

    init_uart();
    initr();
  /* interruption();*/

    IE2 |= UCA0RXIE;
    sendString(strcat(data1,data2));
    sendString(data3);
    __bis_SR_register( LPM3_bits + GIE ); /*Enter LPM0, interrupts enabled*/




    return 0;
}
#pragma vector=USCIAB0RX_VECTOR
__interrupt void receive(void)
{
    while (!(IFG2&UCA0TXIFG)); /* USCI_A0 TX buffer ready, si um send data pc could not receive data from pc?*/
    UCA0TXBUF = UCA0RXBUF; /*TX -&gt; RXed character, ici on copy ce qu on a dans le terminal puty*/
    //P1OUT ^=BIT6;


    if(UCA0RXBUF=='a')
    {


        Avancer();
        sendString("\rAvancer\r\n");

    }
    if(UCA0RXBUF=='b')
    {
           Reculer();
           sendString("\rReculer\r\n");
    }
    if(UCA0RXBUF=='c')
    {

            //P1OUT &=~BIT6;
           TournerDroiteM();
            sendString("\rTouner a droite\r\n");


    }
    if(UCA0RXBUF=='d')
    {
        TournerGaucheM();
        sendString("\rTourner a gauche\r\n");

    }
    if(UCA0RXBUF=='e')
    {
            Stop();
            sendString("\rArret\r\n");

    }

    if(UCA0RXBUF=='h')
    {


              sendString(strcat(data1,data2));
              sendString(data3);
   }


    IFG2&=~UCA0RXIFG;

}

