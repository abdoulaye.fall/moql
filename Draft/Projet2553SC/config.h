/*
 * config.h
 *
 *  Created on: 11 mars 2021
 *      Author: layef
 */

void init_BOARD( void );
void init_UART( void );
void init_USCI( void );

void envoi_msg_UART(unsigned char * );
void Send_char_SPI( unsigned char );
