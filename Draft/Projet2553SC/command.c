
#include <msp430.h>
#include <string.h>
#include "command.h"
#include "define.h"
#include "deplacement.h"

/*
 * Prototypes
 */

unsigned char cmd[CMDLEN];      // tableau de caracteres lie a la commande user

/* ----------------------------------------------------------------------------
 * Fonction d'interpretation des commandes utilisateur
 * Entrees: -
 * Sorties:  -
 */
void interpreteur( void )
{
    if(strcmp((const char *)cmd, "h") == 0)          //----------------------------------- help
    {
        envoi_msg_UART("\r\nCommandes :");
        envoi_msg_UART("\r\n'ver' : version");
        envoi_msg_UART("\r\n'0' : LED off");
        envoi_msg_UART("\r\n'1' : LED on");
        envoi_msg_UART("\r\n'2' : LED RED on");
        envoi_msg_UART("\r\n'3' : LED GREEN on");
        envoi_msg_UART("\r\n'4' : LED GREEN off");
        envoi_msg_UART("\r\n'5' : LED RED off");
        envoi_msg_UART("\r\n'6' : active synchronize");
        envoi_msg_UART("\r\n'h' : help\r\n");
    }
    else if (strcmp((const char *)cmd, "0") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((unsigned char *)cmd);
        envoi_msg_UART("->");
        Send_char_SPI(0x30); // Send '0' over SPI to Slave
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char *)cmd, "1") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((unsigned char *)cmd);
        envoi_msg_UART("->");
        Send_char_SPI(0x31); // Send '1' over SPI to Slave
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char *)cmd, "2") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((unsigned char *)cmd);
        envoi_msg_UART("->");
        Send_char_SPI(0x32); // Send '1' over SPI to Slave
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char *)cmd, "3") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((unsigned char *)cmd);
        envoi_msg_UART("->");
        Send_char_SPI(0x33); // Send '1' over SPI to Slave
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char *)cmd, "4") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((unsigned char *)cmd);
        envoi_msg_UART("->");
        Send_char_SPI(0x34); // Send '1' over SPI to Slave
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char *)cmd, "5") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((unsigned char *)cmd);
        envoi_msg_UART("->");
        Send_char_SPI(0x35); // Send '1' over SPI to Slave
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char *)cmd, "6") == 0)
     {

     }
    else if (strcmp((const char *)cmd, "ver") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART(RELEASE);
        envoi_msg_UART("\r\n");
    }

    else                          //---------------------------- default choice
    {
        envoi_msg_UART("\r\n ?");
        envoi_msg_UART((unsigned char *)cmd);
    }
    envoi_msg_UART(PROMPT);        //---------------------------- command prompt
}
void interpreteurblut( void )
{
    if(strcmp((const char *)cmd, "h") == 0)          //----------------------------------- help
    {
        envoi_msg_UART("\r\nCommandes :");
        envoi_msg_UART("\r\n'ver' : version");
        envoi_msg_UART("\r\n'a' : advance");
        envoi_msg_UART("\r\n'b' : back");
        envoi_msg_UART("\r\n'l' : left");
        envoi_msg_UART("\r\n'r' : right");
        envoi_msg_UART("\r\n's' : stop");
        envoi_msg_UART("\r\n'v' : start servo");
        envoi_msg_UART("\r\n't' : stop servo");
        envoi_msg_UART("\r\n'h' : help\r\n");
    }
    else if (strcmp((const char *)cmd, "v") == 0)
      {
          envoi_msg_UART("\r\n");
          envoi_msg_UART((unsigned char *)cmd);
          envoi_msg_UART("->");
          Send_char_SPI(0x31); // Send '0' over SPI to Slave
          envoi_msg_UART("\r\n");
      }
      else if (strcmp((const char *)cmd, "t") == 0)
      {
          envoi_msg_UART("\r\n");
          envoi_msg_UART((unsigned char *)cmd);
          envoi_msg_UART("->");
          Send_char_SPI(0x30); // Send '1' over SPI to Slave
          envoi_msg_UART("\r\n");
      }
    else if (strcmp((const char *)cmd, "a") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((unsigned char *)cmd);
        envoi_msg_UART("->");
        Avancer();
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char *)cmd, "b") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((unsigned char *)cmd);
        envoi_msg_UART("->");
        Reculer();
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char *)cmd, "l") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((unsigned char *)cmd);
        envoi_msg_UART("->");
        TournerGaucheM();
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char *)cmd, "r") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((unsigned char *)cmd);
        envoi_msg_UART("->");
        TournerDroiteM();
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char *)cmd, "s") == 0)
   {
       envoi_msg_UART("\r\n");
       envoi_msg_UART((unsigned char *)cmd);
       envoi_msg_UART("->");
       Stop();
       envoi_msg_UART("\r\n");
   }
    else
    {

        envoi_msg_UART("\r\n ?");
        envoi_msg_UART((unsigned char *)cmd);
    }

    envoi_msg_UART(PROMPT);        //---------------------------- command prompt
}






