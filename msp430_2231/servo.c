
#include "msp430.h"
#include "servo.h"
#include "detection.h"

void initservo(void)
{
      WDTCTL = WDTPW + WDTHOLD;



       P1DIR |= BIT6;                          /* P1.6/TA0.1 is used for PWM, thus also an output -> servo 1 */


       P1OUT = 0;                              /* Clear all outputs P1 */
       P2OUT = 0;                              /* Clear all outputs P2 */

       P1SEL |= BIT6;                          /* P1.6 select TA0.1 option */


       /* if SMCLK is about 1MHz (or 1000000Hz),
       * and 1000ms are the equivalent of 1 Hz,
       * then, by setting CCR0 to 20000 (1000000 / 1000 * 20)
       * we get a period of 20ms
       * TA0CCR0 = 20000-1;                            PWM Period TA0.1 */



       TA0CCTL1 = OUTMOD_7;                       /*CCR1 reset/set*/
       TA0CTL   = TASSEL_2 + MC_1;                /*SMCLK, up mode*/
       TA0CTL |=TAIE;
}
void rotation(void)
{
    /*setting 1500 is 1.5ms is 0deg. servo pos
     *TA0CCR1 = 1500;                            CR1 PWM duty cycle */

            __delay_cycles(2500);
            TA0CCR1 = 1000;


            __delay_cycles(2500);
            TA0CCR1 = 1500;


            __delay_cycles(2500);
            TA0CCR1 = 2000;
            

            __delay_cycles(2500);
            TA0CCR1 = 1500;




}
#pragma vector = TIMER0_A1_VECTOR
__interrupt void timer(void)
{
    while(1)
       {
           if (detectionObstacle()== 1U)
             {
                 P1OUT |=BIT0;
                 TA0CCR1 =0;
             }
             else
             {
                 rotation();
                 P1OUT &=~BIT0;
             }
       }

}


